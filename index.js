/**
 * Bài 1: Tính tiền lương nhân viên
 *
 * Đầu vào: Số ngày làm
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho số ngày làm workDays
 * Bước 2: Tạo biến cho tiền lương nhân viên employeeSalary
 * Bước 3: Gán giá trị cho workDays
 * Bước 4: Sử dụng công thức employeeSalary = workDays * 100000
 * Bước 5: In kết quả ra console
 *
 * Đầu ra: Tiền lương nhân viên */

function calculateSalary() {
  var workDays = document.getElementById("work-days").value * 1;
  var daySalary = document.getElementById("day-salary").value * 1;
  document.getElementById("result-1").innerText = "👉 " + workDays * daySalary;
}

/**
 * Bài 2: Tính giá trị trung bình
 *
 * Đầu vào: 5 số thực
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho 5 số thực num1, num2, num3, num4, num5
 * Bước 2: Tạo biến cho giá trị trung bình average
 * Bước 3: Gán giá trị cho num1, num2, num3, num4, num5
 * Bước 4: Sử dụng công thức average = (num1 + num2 + num3 + num4 + num5) / 5
 * Bước 5: In kết quả ra console
 *
 * Đầu ra: Giá trị trung bình của 5 số thực */

function calculateAverage() {
  var num1 = document.getElementById("num-1").value * 1;
  var num2 = document.getElementById("num-2").value * 1;
  var num3 = document.getElementById("num-3").value * 1;
  var num4 = document.getElementById("num-4").value * 1;
  var num5 = document.getElementById("num-5").value * 1;
  document.getElementById("result-2").innerText =
    "👉 " + (num1 + num2 + num3 + num4 + num5) / 5;
}

/**
 * Bài 3: Tính và xuất ra số tiền sau quy đổi VND
 *
 * Đầu vào: Số tiền USD
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho số tiền USD usdAmount
 * Bước 2: Tạo biến cho số tiền VND vndAmount
 * Bước 3: Gán giá trị cho usdAmount
 * Bước 4: Sử dụng công thức vndAmount = usdAmount * 23500
 * Bước 5: In kết quả ra console
 *
 * Đầu ra: Số tiền sau quy đổi VND */

function calculateVND() {
  var usdAmount = document.getElementById("usd-amount").value * 1;
  document.getElementById("result-3").innerText = "👉 " + usdAmount * 23500;
}

/**
 * Bài 4: Tính và xuất ra diện tích, chu vi của hình chữ nhật
 *
 * Đầu vào: Chiều dài và chiều rộng của hình chữ nhật
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho chiều dài length và chiều rộng width của hình chữ nhật
 * Bước 2: Tạo biến cho diện tích area và chu vi perimeter của hình chữ nhật
 * Bước 3: Gán giá trị cho length và width
 * Bước 4: Sử dụng công thức area = length * width, perimeter = (length + width) * 2
 * Bước 5: In kết quả ra console
 *
 * Đầu ra: Diện tích và chu vi của hình chữ nhật */

function calculateAreaPerimeter() {
  var length = document.getElementById("length").value * 1;
  var width = document.getElementById("width").value * 1;
  document.getElementById("result-4").innerText =
    "👉 Diện tích: " + length * width + "; Chu vi: " + (length + width) * 2;
}

/**
 * Bài 5: Tính tổng 2 ký số
 *
 * Đầu vào: 1 số có 2 chữ số
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho 1 số có 2 chữ số num
 * Bước 2: Tạo biến cho số hàng đơn vị units và số hàng chục tens
 * Bước 3: Tạo biến cho tổng 2 ký số của số vừa nhập total
 * Bước 4: Gán giá trị cho num
 * Bước 5: Sử dụng công thức units = num % 10, tens = Math.floor(num / 10)
 * Bước 6: Sử dụng công thức total = units + tens
 * Bước 7: In kết quả ra console
 *
 * Đầu ra: Tổng 2 ký số */

function calculateSum() {
  var num = document.getElementById("num").value * 1;
  var units = num % 10;
  var tens = Math.floor(num / 10);
  document.getElementById("result-5").innerText = "👉 " + (units + tens);
}
